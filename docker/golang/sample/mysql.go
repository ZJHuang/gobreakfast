package main

import (
    "fmt"
    "net/http"
    "log"
    "gorm.io/gorm"
    "encoding/json"
    "gorm.io/driver/mysql"
)

type Order struct {
//     gorm.Model
    ID int
    ORDER_ID string
}

const (
	UserName     string = "root"
	Password     string = "root"
	Addr         string = "127.0.0.1"
	Port         int    = 3306
	Database     string = "go_breakfast"
	MaxLifetime  int    = 10
	MaxOpenConns int    = 10
	MaxIdleConns int    = 10
)

func ConnectDB () string {
 addr := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8&parseTime=True", UserName, Password, Addr, Port, Database)
 db, err := gorm.Open(mysql.Open(addr), &gorm.Config{})
         if err != nil {
             panic("failed to connect database")
             return ""
         }

           // Read
            var order Order
            db.Table("order").First(&order, 1) // find product with integer primary key
           json_product, err := json.Marshal(order)
           if err != nil {
               fmt.Println(err)
               return ""
           }

           fmt.Printf("%+v\n", order)
           fmt.Printf("%+v\n", string(json_product))
           return string(json_product)
}

func main () {
    json_product := ConnectDB()
    http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
        fmt.Fprintf(w, "Hello 123333111")
        fmt.Fprintf(w, json_product)
    })
    log.Fatal(http.ListenAndServe(":8083", nil))
}